import firebase from 'firebase/app'
import 'firebase/firestore' 
import 'firebase/auth'

const firebaseConfig = {
    apiKey: "AIzaSyDAspLjrVpxK5UHlxoopaT8t9-8UMzXVL0",
    authDomain: "chatcontext-cursoreact.firebaseapp.com",
    projectId: "chatcontext-cursoreact",
    storageBucket: "chatcontext-cursoreact.appspot.com",
    messagingSenderId: "223669093834",
    appId: "1:223669093834:web:2902c4c62cb9a3d05d3b33"
  };
  
  // Initialize Firebase
firebase.initializeApp(firebaseConfig);

const db = firebase.firestore()
const auth = firebase.auth()

const provider = new firebase.auth.GoogleAuthProvider()


export {db, auth, provider}