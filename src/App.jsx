import {ChatContext} from './context/ChatProvider'
import {useContext} from 'react'
import Navbar from './components/Navbar';
import Chat from './components/Chat';

function App() {

  const {usuario} = useContext(ChatContext)


  return usuario!== null ? (
    <div className="App">
      <Navbar />
      {
        usuario.estado ? (
          <Chat />
        ) : (
          <div className="lead text-center mt-5">Debes iniciar sesión</div>
        )
      }
    </div>
  ):(
    <div>Loading...</div>
  )
  ;
}

export default App;
