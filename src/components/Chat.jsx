import React, {useContext, useRef, useEffect} from 'react'

import {ChatContext} from '../context/ChatProvider'
import Agregar from './Agregar'

const Chat = () => {

    const {usuario, mensajes} = useContext(ChatContext)
    const refZonaChat = useRef(null) //el ref es como usar un ID para acceder a todo ese div

    useEffect(() => {
        if(refZonaChat.current !== null){
          refZonaChat.current.scrollTop = refZonaChat.current.scrollHeight;
        }
      }, [mensajes])

    return (
        <div 
            className='mt-3 px-2' 
            ref={refZonaChat} 
            style={{ height: '75vh', overflowY: 'scroll'}}
        >

            {
                mensajes.map((item, index) => (
                    item.uid === usuario.uid ? ( //si la misma persona es la que manda el mensaje, se pinta a la derecha, sino es del otro usuario
                        <div className="d-flex justify-content-end mb-2" key={index}>
                            <span className="badge bg-primary rounded-pill float-end">{item.texto}</span>
                        </div>
                    ) : (
                        <div className="d-flex justify-content-start mb-2" key={index}>
                            <span className='badge bg-primary rounded-pill float-start'>{item.texto}</span>
                        </div>
                    )
                ))
            }

            <Agregar />
        
        </div>
    )
}

export default Chat
